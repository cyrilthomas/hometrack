import path from 'path';
import gulp from 'gulp';
import babel from 'gulp-babel';
import runSequence from 'run-sequence';
import del from 'del';
import nodemon from 'gulp-nodemon';

gulp.task('default', ['develop']);

gulp.task('develop', () =>
    nodemon({
        script: './server/index.js',
        exec: path.resolve('./node_modules/.bin/babel-node'),
        ext: 'js styl yaml pug',
        env: {
            NODE_ENV: 'development'
        },
        delay: 2000,
        nodeArgs: [
            '--use-strict'
        ],
        watch: [
            'server/**/*',
            'config/**/*'
        ]
    }),
);

gulp.task('build-server-production', () =>
    gulp.src([
        './server/**/*.js'
    ])
        .pipe(babel())
        .pipe(gulp.dest('./build/server')),
);

gulp.task('build-app-production', () =>
    gulp.src([
        './app/**/*.js'
    ])
        .pipe(babel())
        .pipe(gulp.dest('./build/app')),
);

gulp.task('build-config-production', () =>
    gulp.src([
        './config/**/*.*'
    ])
        .pipe(gulp.dest('./build/config')),
);

gulp.task('copy-misc', () =>
    gulp.src([
        './package.json',
        './.gitignore'
    ])
        .pipe(gulp.dest('./build')),
);

gulp.task('clean-build', () =>
    del.sync(['./build/**/*']),
);

gulp.task('build', (done) =>
    runSequence(
        'clean-build',
        [
            'build-server-production',
            'build-app-production',
            'build-config-production',
            'copy-misc'
        ],
        done,
    ),
);

/* eslint import/no-extraneous-dependencies:0 */
