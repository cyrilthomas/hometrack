import './env';
import config from 'config';
import log from './log';

const app = require('./app');

app.context.log = log;

process.env.PORT = config.app.port;
app.listen(config.app.port, (err) => {
    if (err) throw err;
    log.info(`Listening on port ${config.app.port} 🍿 `, {
        APP_ENV: process.env.APP_ENV,
        NODE_ENV: process.env.NODE_ENV
    });
});

/* eslint import/first:0 */
