type PayloadItemType = {
    type: 'string',
    workflow: 'string',
    address: {
        buildingNumber: 'string',
        street: string,
        suburb: string,
        state: string,
        postcode: string
    }
}

export type PayloadType = Array<PayloadItemType>
