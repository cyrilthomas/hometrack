// @flow
import type { PayloadType } from '../types/payload';

export default (payload: PayloadType) => (
    payload
        .filter((item) => (
            item.type.toLowerCase() === 'htv' &&
            item.workflow.toLowerCase() === 'completed'
        ))
        .map((item) => {
            const { type, workflow, address: { buildingNumber, street, suburb, state, postcode } } = item;
            const concataddress = [buildingNumber, street, suburb, state, postcode].join(' ');
            return { concataddress, type, workflow };
        })
);
