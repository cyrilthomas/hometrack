import config from 'config';
import winston from 'winston';
import debug from 'debug';

const console = new winston.transports.Console({
    level: config.logging.level,
    timestamp: config.logging.timestamps
});

const logger = new winston.Logger({
    exitOnError: false,
    transports: [
        console
    ]
});

if (config.logging.cli) {
    logger.cli();
}

// Set debugging level.
if (!process.env.DEBUG && config.logging.debug) {
    debug.enable(config.logging.debug);
}

function logError(error) {
    // We don't want these logged in production.
    if (error.status === 404 && process.env.NODE_ENV === 'production') {
        return;
    }

    logger.error(error.message, { ...error, stack: error.stack ? `\n${error.stack}` : null });
}

function debugFactory(name) {
    const log = debug(`app:${name}`);
    return log;
}

export { logger as default, logError, debugFactory as debugLogger };
