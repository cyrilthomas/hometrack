import htv from '../../lib/htv';
import throwApiError from '../../util/throw-api-error';

export default async (ctx) => {
    const { body } = ctx.request;
    if (!Array.isArray(body.payload)) throwApiError(ctx);

    ctx.body = { response: htv(body.payload) };
};
