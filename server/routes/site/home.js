import { markdown } from 'markdown';
import { readFileSync } from 'fs';

const readmeString = readFileSync(`${__dirname}/../../../README.md`, 'utf8');
const readme = markdown.toHTML(readmeString);

export default (ctx) => {
    ctx.body = readme;
};
