export default (ctx) => {
    ctx.throw(400, 'Could not decode request: JSON parsing failed');
};
