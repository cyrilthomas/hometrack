describe('htv', () => {
    it('shows only completed workflows', () => {
        const htv = require('../../../lib/htv');
        const payload = [
            {
                type: 'htv',
                workflow: 'completed',
                address: {}
            },
            {
                type: 'htv',
                workflow: 'pending',
                address: {}
            },
            {
                type: 'htv',
                workflow: 'completed',
                address: {}
            }
        ];

        const actual = htv(payload);
        expect(actual).toHaveLength(2);
    });

    it('shows only htv type', () => {
        const htv = require('../../../lib/htv');
        const payload = [
            {
                type: 'htv',
                workflow: 'completed',
                address: {}
            },
            {
                type: 'avm',
                workflow: 'completed',
                address: {}
            },
            {
                type: 'avm',
                workflow: 'completed',
                address: {}
            }
        ];

        const actual = htv(payload);
        expect(actual).toHaveLength(1);
    });

    it('concatanates all the address fields and strips additional fields', () => {
        const htv = require('../../../lib/htv');
        const payload = [
            {
                type: 'htv',
                additional: ['a', 'b', 'c'],
                workflow: 'completed',
                address: {
                    buildingNumber: '100',
                    street: 'Amber Close',
                    suburb: 'Alpine',
                    state: 'NSW',
                    postcode: '2575'
                }
            },
            {
                type: 'avm',
                additional: ['a', 'b', 'c'],
                workflow: 'completed',
                address: {
                    buildingNumber: '228',
                    street: 'Allgomera Road',
                    suburb: 'Allgomera',
                    state: 'NSW',
                    postcode: '2441'
                }
            },
            {
                type: 'htv',
                additional: ['a', 'b', 'c'],
                workflow: 'completed',
                address: {
                    buildingNumber: '228',
                    street: 'Allgomera Road',
                    suburb: 'Allgomera',
                    state: 'NSW',
                    postcode: '2441'
                }
            }
        ];

        const actual = htv(payload);
        const expected = [
            {
                concataddress: '100 Amber Close Alpine NSW 2575',
                type: 'htv',
                workflow: 'completed'
            },
            {
                concataddress: '228 Allgomera Road Allgomera NSW 2441',
                type: 'htv',
                workflow: 'completed'
            }
        ];
        expect(actual).toEqual(expected);
    });
});
