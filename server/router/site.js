import home from '../routes/site/home';

const router = require('koa-router')();

router.get('home', '/', home);

export default router;
