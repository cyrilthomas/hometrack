import bodyParser from 'koa-bodyparser';
import errors from '../middleware/errors';
import throwApiError from '../util/throw-api-error';
import htv from '../routes/api/htv';

const router = require('koa-router')({ prefix: '/api' });

router.use(errors);
router.use(bodyParser({ onerror: (err, ctx) => throwApiError(ctx) }));

router.post('htv', '/htv', htv);

export default router;
