import config from 'config';
import Koa from 'koa';
import conditional from 'koa-conditional-get';
import logger from 'koa-logger';
import cache from 'koa-cache-control';
import convert from 'koa-convert';
import { logError } from './log';
import apiRouter from './router/api';
import siteRouter from './router/site';

const app = new Koa();
module.exports = app;

app.keys = ['some secret hurr'];
app.proxy = config.app.proxy;

// app context
Object.assign(app.context, {
    isDevelopment() {
        return (!process.env.NODE_ENV || process.env.NODE_ENV === 'development');
    }
});

// request logging
if (config.logging.requests) {
    app.use(convert(logger()));
}

// app cache
app.use(convert(cache({
    // public: true,
    // maxAge: 20 * 60,
    noCache: true
})));

app.use(convert(conditional()));

app.use(siteRouter.routes());
app.use(apiRouter.routes());


app.on('error', (err) => { logError(err); });
