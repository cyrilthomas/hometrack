# Hometrack API

## Endpoints

`POST` - [/api/htv](/api/htv)

This endpoint would filter only htv completed workflows with concatenated address from the payload

* [request sample](https://raw.githubusercontent.com/Hometrack/codetest/master/hometrack-sample-request.json)
* [response sample](https://raw.githubusercontent.com/Hometrack/codetest/master/hometrack-sample-response.json)

## Tests

```
npm run test
```

## Running locally

```
npm run dev
```